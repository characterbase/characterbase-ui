import {CharactersService} from "../services/characters.service";
import {Character} from "../types/character";

let charactersStore: Character[] = [];

export class CharactersRepository {
    private charactersService: CharactersService;

    constructor() {
        this.charactersService = new CharactersService();
    }

    public async getCharacters(): Promise<Character[]> {
        if (charactersStore.length > 0) {
            return Promise.resolve(charactersStore);
        }

        return charactersStore = await this.charactersService.getCharacters();
    }

    public async addCharacter(character: Character): Promise<Character> {
        const addedCharacter = await this.charactersService.createCharacter(character);
        charactersStore = [...charactersStore, addedCharacter];

        return addedCharacter;
    }

    public async updateCharacter(character: Character): Promise<Character> {
        const updatedCharacter = await this.charactersService.updateCharacter(character);
        const filterOtherCharacters = (otherCharacter: Character): boolean => {
            return otherCharacter.id !== updatedCharacter.id;
        };
        const otherCharacters = charactersStore.filter(filterOtherCharacters);
        charactersStore = [...otherCharacters, updatedCharacter];

        return updatedCharacter;
    }

    public async removeCharacter(character: Character): Promise<void> {
        await this.charactersService.deleteCharacter(character);
        const filterOtherCharacters = (otherCharacter: Character): boolean => {
            return otherCharacter.id !== character.id;
        };
        const otherCharacters = charactersStore.filter(filterOtherCharacters);
        charactersStore = [...otherCharacters];
    }
}
