import {Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterAbilityScores} from "../../types/character-ability-scores";
import {CharacterAbilityScoresFieldComponent} from "./character-ability-scores-field.component";
import {characterAbilityScoresStyles} from "./character-ability-scores.styles";

interface IProps extends WithStyles<typeof characterAbilityScoresStyles> {
    abilityScores: CharacterAbilityScores;
    newCharacter: boolean;
    onAbilityScoresUpdated: (characterStats: CharacterAbilityScores) => void;
}

class CharacterAbilityScoresComponent extends React.Component<IProps> {
    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterAbilityScores}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Ability Scores
                </Typography>

                <Grid container
                      direction="column">
                    <Grid item
                          sm={12}
                          className={this.props.classes.characterAbilityScoresField}>
                        <CharacterAbilityScoresFieldComponent abilityScore={this.props.abilityScores.strength}
                                                              label="Strength"
                                                              fullWidth={true}
                                                              showStepper={this.props.newCharacter}
                                                              onChangeHandler={this.updateStrength} />
                    </Grid>

                    <Divider />

                    <Grid item
                          sm={12}
                          className={this.props.classes.characterAbilityScoresField}>
                        <CharacterAbilityScoresFieldComponent abilityScore={this.props.abilityScores.constitution}
                                                              label="Constitution"
                                                              fullWidth={true}
                                                              showStepper={this.props.newCharacter}
                                                              onChangeHandler={this.updateConstitution} />
                    </Grid>

                    <Divider />

                    <Grid item
                          sm={12}
                          className={this.props.classes.characterAbilityScoresField}>
                        <CharacterAbilityScoresFieldComponent abilityScore={this.props.abilityScores.dexterity}
                                                              label="Dexterity"
                                                              fullWidth={true}
                                                              showStepper={this.props.newCharacter}
                                                              onChangeHandler={this.updateDexterity} />
                    </Grid>

                    <Divider />

                    <Grid item
                          sm={12}
                          className={this.props.classes.characterAbilityScoresField}>
                        <CharacterAbilityScoresFieldComponent abilityScore={this.props.abilityScores.intelligence}
                                                              label="Intelligence"
                                                              fullWidth={true}
                                                              showStepper={this.props.newCharacter}
                                                              onChangeHandler={this.updateIntelligence} />
                    </Grid>

                    <Divider />

                    <Grid item
                          sm={12}
                          className={this.props.classes.characterAbilityScoresField}>
                        <CharacterAbilityScoresFieldComponent abilityScore={this.props.abilityScores.wisdom}
                                                              label="Wisdom"
                                                              fullWidth={true}
                                                              showStepper={this.props.newCharacter}
                                                              onChangeHandler={this.updateWisdom} />
                    </Grid>

                    <Divider />

                    <Grid item
                          sm={12}
                          className={this.props.classes.characterAbilityScoresField}>
                        <CharacterAbilityScoresFieldComponent abilityScore={this.props.abilityScores.charisma}
                                                              label="Charisma"
                                                              fullWidth={true}
                                                              showStepper={this.props.newCharacter}
                                                              onChangeHandler={this.updateCharisma} />
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    private updateStrength = (strValue: number): void => {
        this.updateAbilityScore({strength: strValue} as CharacterAbilityScores);
    }

    private updateConstitution = (conValue: number): void => {
        this.updateAbilityScore({constitution: conValue} as CharacterAbilityScores);
    }

    private updateDexterity = (dexValue: number): void => {
        this.updateAbilityScore({dexterity: dexValue} as CharacterAbilityScores);
    }

    private updateIntelligence = (intValue: number): void => {
        this.updateAbilityScore({intelligence: intValue} as CharacterAbilityScores);
    }

    private updateWisdom = (wisValue: number): void => {
        this.updateAbilityScore({wisdom: wisValue} as CharacterAbilityScores);
    }

    private updateCharisma = (chaValue: number): void => {
        this.updateAbilityScore({charisma: chaValue} as CharacterAbilityScores);
    }

    private updateAbilityScore = (abilityScore: { [key in keyof CharacterAbilityScores]: number }): void => {
        this.props.onAbilityScoresUpdated(Object.assign(this.props.abilityScores, abilityScore));
    }
}

const characterAbilityScoresComponent = withStyles(characterAbilityScoresStyles)(CharacterAbilityScoresComponent);

export {characterAbilityScoresComponent as CharacterAbilityScoresComponent};
