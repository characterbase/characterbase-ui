import {createStyles} from "@material-ui/core";

export const characterArmorClassStyles = createStyles({
    characterArmorClass: {
        padding: 8
    },
    characterArmorClassField: {
        padding: 10
    }
});
