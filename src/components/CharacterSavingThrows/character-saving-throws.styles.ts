import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";
import {CharacterBaseTheme} from "../App/app.theme";

export const characterSavingThrowsStyles = (theme: CharacterBaseTheme): StyleRules => createStyles({
    characterSavingThrows: {
        padding: 8
    },
    characterSavingThrowsCheckbox: {
        padding: 0
    },
    characterSavingThrowsField: {
        padding: "8px 0"
    },
    characterSavingThrowsTextField: {
        paddingLeft: theme.spacing.unit
    }
});
