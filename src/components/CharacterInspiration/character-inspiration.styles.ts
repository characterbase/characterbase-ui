import {createStyles} from "@material-ui/core";

export const characterInspirationStyles = createStyles({
    characterInspiration: {
        padding: 8
    },
    characterInspirationCheckbox: {
        padding: 0,
        width: "100%"
    },
    characterInspirationField: {
        padding: 10
    }
});
