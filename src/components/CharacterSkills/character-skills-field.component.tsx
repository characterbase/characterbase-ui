import {Checkbox, FormControl, Grid, TextField, withStyles, WithStyles} from "@material-ui/core";
import {RadioButtonChecked, RadioButtonUnchecked} from "@material-ui/icons";
import * as React from "react";
import {CharacterSkill} from "../../types/character-skills";
import {CharacterStatBonus} from "../../types/character-stat-bonus";
import {characterSkillsStyles} from "./character-skilsl.styles";

type Props = WithStyles<typeof characterSkillsStyles> & {
    label: string;
    proficiencyBonus: number;
    skill: CharacterSkill;
};

class CharacterSkillsFieldComponent extends React.Component<Props> {
    private readonly modifier: number;

    constructor(props: Props) {
        super(props);

        const reduceBonusSeed = props.skill.proficient ? props.proficiencyBonus : 0;

        this.modifier = props.skill
            .bonuses
            .reduce(this.reduceBonus, reduceBonusSeed);
    }

    public render(): JSX.Element {
        const modifierPrefix = this.modifier >= 0 ? "+" : "";

        return (
            <FormControl>
                <Grid container
                      alignItems="baseline">
                    <Grid item
                          xs={2}>
                        <Checkbox checked={this.props.proficiencyBonus > 0}
                                  checkedIcon={<RadioButtonChecked />}
                                  className={this.props.classes.characterSkillsCheckbox}
                                  color="primary"
                                  icon={<RadioButtonUnchecked />}
                                  readOnly={true} />
                    </Grid>

                    <Grid item
                          xs={10}>
                        <TextField className={this.props.classes.characterSkillsTextField}
                                   fullWidth={true}
                                   InputProps={{disableUnderline: true, readOnly: true}}
                                   label={this.props.label}
                                   value={`${modifierPrefix}${this.modifier}`} />
                    </Grid>
                </Grid>
            </FormControl>
        );
    }

    private reduceBonus = (previousModifier: number, currentBonus: CharacterStatBonus): number => {
        return previousModifier + currentBonus.modifier;
    }
}

const characterSkillsComponent = withStyles(characterSkillsStyles)(CharacterSkillsFieldComponent);

export {characterSkillsComponent as CharacterSkillsFieldComponent};
