import {createStyles} from "@material-ui/core";

export const characterAttackStyles = createStyles({
    characterAttacks: {
        padding: 8
    },
    characterAttacksField: {
        padding: 10
    }
});
