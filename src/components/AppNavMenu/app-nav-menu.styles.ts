import {createStyles} from "@material-ui/core";
import {CharacterBaseTheme} from "../App/app.theme";

export const appNavMenuStyles = (theme: CharacterBaseTheme) => createStyles({
    appNavGridContainer: {
        height: `calc(100vh - 48px)`,
    },
    appNavMenu: {
        height: "100%",
        minWidth: theme.appNav.width
    },
    appNavMenuAppBar: {
        position: "relative"
    },
    channelsMenu: {
        backgroundColor: "#f5eeed",
        height: "100%",
        position: "relative",
        zIndex: 0
    },
    groupsMenu: {
        backgroundColor: "#e2cfcc",
        height: "100%",
        position: "relative",
        zIndex: 0
    }
});
