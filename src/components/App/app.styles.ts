import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";

export const appStyles = (): StyleRules => createStyles({
    app: {
        display: "flex",
        overflow: "hidden"
    }
});
