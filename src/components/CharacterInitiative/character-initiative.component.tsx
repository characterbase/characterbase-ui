import {Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {characterInitiativeStyles} from "./character-initiative.styles";

type Props = WithStyles<typeof characterInitiativeStyles> & {
    initiative: number;
};

const CharacterInitiativeComponent: React.SFC<Props> = (props: Props): JSX.Element => {
    return (
        <Paper className={props.classes.characterInitiative}>
            <Typography color="primary"
                        component="legend"
                        variant="overline">
                Initiative
            </Typography>

            <Grid container>
                <Grid className={props.classes.characterInitiativeField}
                      item
                      xs={12}>
                    <Typography align="center"
                                variant="h5">
                        {`${props.initiative >= 0 ? "+" : ""}${props.initiative}`}
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    );
};

const characterInitiativeComponent = withStyles(characterInitiativeStyles)(CharacterInitiativeComponent);

export {characterInitiativeComponent as CharacterInitiativeComponent};
