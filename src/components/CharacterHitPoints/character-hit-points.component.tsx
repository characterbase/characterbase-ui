import {Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterHitPoints} from "../../types/character-hit-points";
import {CharacterTextFieldComponent} from "../CharacterTextField/character-text.field.component";
import {characterHitPointsStyles} from "./character-hit-points.styles";

type Props = WithStyles<typeof characterHitPointsStyles> & {
    hitPoints: CharacterHitPoints;
    onHitPointsUpdated: (hitPoints: CharacterHitPoints) => void;
};

class CharacterHitPointsComponent extends React.Component<Props> {
    public handleCurrentHitPointsUpdated = (currentHitPoints: number): void => {
        this.props.onHitPointsUpdated({...this.props.hitPoints, current: currentHitPoints});
    }

    public handleTemporaryHitPointsUpdated = (temporaryHitPoints: number): void => {
        this.props.onHitPointsUpdated({...this.props.hitPoints, temporary: temporaryHitPoints});
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterHitPoints}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Hit Points
                </Typography>

                <Grid container
                      direction="column">
                    <Grid className={this.props.classes.characterHitPointsField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.hitPoints.maximum}
                                                     fullWidth={true}
                                                     label="Maximum"
                                                     readOnly={true} />
                    </Grid>

                    <Divider />

                    <Grid className={this.props.classes.characterHitPointsField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.hitPoints.current}
                                                     fullWidth={true}
                                                     label="Current"
                                                     max={this.props.hitPoints.maximum}
                                                     min={0}
                                                     onChangeHandler={this.handleCurrentHitPointsUpdated}
                                                     readOnly={false} />
                    </Grid>

                    <Grid className={this.props.classes.characterHitPointsField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.hitPoints.temporary}
                                                     fullWidth={true}
                                                     label="Temporary"
                                                     onChangeHandler={this.handleTemporaryHitPointsUpdated}
                                                     readOnly={false} />
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

const characterHitPointsComponent = withStyles(characterHitPointsStyles)(CharacterHitPointsComponent);

export {characterHitPointsComponent as CharacterHitPointsComponent};
