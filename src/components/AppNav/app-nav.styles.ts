import {createStyles} from "@material-ui/core";
import {CharacterBaseTheme} from "../App/app.theme";

export const appNavStyles = (theme: CharacterBaseTheme) => createStyles({
    appNav: {
        [theme.breakpoints.up("sm")]: {
            flexShrink: 0,
            width: theme.appNav.width
        }
    },
    appNavDrawerPaper: {
        borderRight: "none"
    }
});
