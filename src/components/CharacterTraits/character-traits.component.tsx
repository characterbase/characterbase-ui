import {Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterTraits} from "../../types/character-traits";
import {CharacterTextFieldComponent} from "../CharacterTextField/character-text.field.component";
import {characterTraitsStyles} from "./character-traits.styles";

interface IProps extends WithStyles<typeof characterTraitsStyles> {
    traits: CharacterTraits;
    newCharacter: boolean;
    onTraitsUpdated: (traits: CharacterTraits) => void;
}

class CharacterTraitsComponent extends React.Component<IProps> {
    public updateCharacterPersonalityTraits = (updatedPersonalityTraits: string): void => {
        this.updateCharacterTraits({personalityTraits: updatedPersonalityTraits} as CharacterTraits);
    }

    public updateCharacterIdeals = (updatedIdeals: string): void => {
        this.updateCharacterTraits({ideals: updatedIdeals} as CharacterTraits);
    }

    public updateCharacterBonds = (updatedBonds: string): void => {
        this.updateCharacterTraits({bonds: updatedBonds} as CharacterTraits);
    }

    public updateCharacterFlaws = (updatedFlaws: string): void => {
        this.updateCharacterTraits({flaws: updatedFlaws} as CharacterTraits);
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterTraits}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Traits
                </Typography>

                <Grid container>
                    <Grid item
                          xs={12}
                          className={this.props.classes.characterTraitField}>
                        <CharacterTextFieldComponent currentValue={this.props.traits.personalityTraits}
                                                     label="Personality Traits"
                                                     readOnly={!this.props.newCharacter}
                                                     fullWidth={true}
                                                     onChangeHandler={this.updateCharacterPersonalityTraits} />
                    </Grid>

                    <Grid item
                          xs={12}
                          className={this.props.classes.characterTraitField}>
                        <CharacterTextFieldComponent currentValue={this.props.traits.ideals}
                                                     label="Ideals"
                                                     readOnly={!this.props.newCharacter}
                                                     fullWidth={true}
                                                     onChangeHandler={this.updateCharacterIdeals} />
                    </Grid>

                    <Grid item
                          xs={12}
                          className={this.props.classes.characterTraitField}>
                        <CharacterTextFieldComponent currentValue={this.props.traits.bonds}
                                                     label="Bonds"
                                                     readOnly={!this.props.newCharacter}
                                                     fullWidth={true}
                                                     onChangeHandler={this.updateCharacterBonds} />
                    </Grid>

                    <Grid item
                          xs={12}
                          className={this.props.classes.characterTraitField}>
                        <CharacterTextFieldComponent currentValue={this.props.traits.flaws}
                                                     label="Flaws"
                                                     readOnly={!this.props.newCharacter}
                                                     fullWidth={true}
                                                     onChangeHandler={this.updateCharacterFlaws} />
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    private updateCharacterTraits = (updatedTraits: CharacterTraits): void => {
        this.props.onTraitsUpdated({...this.props.traits, updatedTraits} as CharacterTraits);
    }
}

const characterTraitsComponent = withStyles(characterTraitsStyles)(CharacterTraitsComponent);

export {characterTraitsComponent as CharacterTraitsComponent};
