import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";

export const characterTraitsStyles = (): StyleRules => createStyles({
    characterTraitField: {
        padding: 10
    },
    characterTraits: {
        padding: 8
    }
});
