import {
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    Typography,
    withStyles,
    WithStyles
} from "@material-ui/core";
import * as React from "react";
import {Character} from "../../types/character";
import {characterSelectionStyles} from "./character-selection.styles";

interface IProps extends WithStyles<typeof characterSelectionStyles> {
    characters: Character[];
    currentCharacter?: Character;
    onCharacterSelected: (character: Character) => void;
}

type State = {
    value: string;
};

class CharacterSelectionComponent extends React.Component<IProps, State> {
    constructor(props: IProps) {
        super(props);

        const initialStateValue = this.props.currentCharacter != null
            ? this.props.currentCharacter.id
            : "0";

        this.state = {
            value: initialStateValue
        };
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterSelection}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Character Name
                </Typography>

                <Grid container>
                    <Grid className={this.props.classes.characterSelectionField}
                          item
                          xs={12}>
                        <FormControl fullWidth={true}>
                            <InputLabel>Character</InputLabel>

                            <Select onChange={this.handleOnChange}
                                    value={this.state.value}>
                                {this.compileOptions()}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    private compileOptions = (): JSX.Element[] => {
        return this.props
            .characters
            .map(this.mapCharactersToJsx);
    }

    private mapCharactersToJsx = (character: Character): JSX.Element => {
        return (
            <MenuItem key={character.id}
                      value={character.id}>
                {character.details.name || "New Character"}
            </MenuItem>
        );
    }

    private handleOnChange = (event: React.FormEvent): void => {
        const eventTarget = event.target as HTMLSelectElement;
        const selectedCharacter = this.props
            .characters
            .find((character: Character) => this.findCharacterSelectedById(character, eventTarget.value));

        this.setState(() => ({
            value: eventTarget.value
        }));

        if (selectedCharacter != null) {
            this.props.onCharacterSelected(selectedCharacter);
        }
    }

    private findCharacterSelectedById = (character: Character, selectedCharacterId: string): boolean => {
        return character.id === selectedCharacterId;
    }
}

const characterSelectionComponent = withStyles(characterSelectionStyles)(CharacterSelectionComponent);

export {characterSelectionComponent as CharacterSelectionComponent};
