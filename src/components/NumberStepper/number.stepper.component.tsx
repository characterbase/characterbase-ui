import {Button, withStyles, WithStyles} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import * as React from "react";
import {numberStepperStyles} from "./number-stepper.styles";

interface IProps extends WithStyles<typeof numberStepperStyles> {
    value: number;
    step: number;
    min?: number;
    max?: number;
    onStepHandler: (value: number) => void;
}

class NumberStepperComponent extends React.Component<IProps> {
    constructor(props: IProps) {
        super(props);
    }

    public handleOnClickIncrement = (): void => {
        this.props.onStepHandler(this.getMaximumValue(this.props.value));
    }

    public handleOnClickDecrement = (): void => {
        this.props.onStepHandler(this.getMinimumValue(this.props.value));
    }

    public render(): JSX.Element {
        return (
            <div>
                <Button className={`${this.props.classes.stepper} ${this.props.classes.stepperIncrement}`}
                        onClick={this.handleOnClickIncrement}>
                    <AddIcon />
                </Button>

                <Button className={`${this.props.classes.stepper} ${this.props.classes.stepperDecrement}`}
                        onClick={this.handleOnClickDecrement}>
                    <RemoveIcon />
                </Button>
            </div>
        );
    }

    private getMinimumValue = (value: number): number => {
        const decrementedValue = value - this.props.step;

        if (this.props.min == null) {
            return decrementedValue;
        }

        return Math.max(decrementedValue, this.props.min);
    }

    private getMaximumValue = (value: number): number => {
        const incrementedValue = value + this.props.step;

        if (this.props.max == null) {
            return incrementedValue;
        }

        return Math.min(incrementedValue, this.props.max);
    }
}

const numberStepperComponent = withStyles(numberStepperStyles)(NumberStepperComponent);

export {numberStepperComponent as NumberStepperComponent};
