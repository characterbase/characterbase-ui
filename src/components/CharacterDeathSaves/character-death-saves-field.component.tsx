import {Checkbox, FormControl, FormGroup, FormLabel, Grid, withStyles, WithStyles} from "@material-ui/core";
import {RadioButtonChecked, RadioButtonUnchecked} from "@material-ui/icons";
import * as React from "react";
import {NumberStepperComponent} from "../NumberStepper/number.stepper.component";
import {characterDeathSavesStyles} from "./character-death-saves.styles";

type Props = WithStyles<typeof characterDeathSavesStyles> & {
    label: string;
    onDeathSaveUpdated: (deathSaveCount: number) => void;
    saveCount: number;
};

class CharacterDeathSavesFieldComponent extends React.Component<Props> {
    public handleOnChange = (deathSaveCount: number): void => {
        this.props.onDeathSaveUpdated(deathSaveCount);
    }

    public render() {
        return (
            <FormControl fullWidth={true}>
                <FormLabel>{this.props.label}</FormLabel>

                <FormGroup>
                    <Grid container>

                        <Grid item
                              xs={3}>
                            <Checkbox checked={this.props.saveCount > 0}
                                      checkedIcon={<RadioButtonChecked />}
                                      color="primary"
                                      icon={<RadioButtonUnchecked />} />
                        </Grid>

                        <Grid item
                              xs={3}>
                            <Checkbox checked={this.props.saveCount > 1}
                                      checkedIcon={<RadioButtonChecked />}
                                      color="primary"
                                      icon={<RadioButtonUnchecked />} />
                        </Grid>

                        <Grid item
                              xs={3}>
                            <Checkbox checked={this.props.saveCount > 2}
                                      checkedIcon={<RadioButtonChecked />}
                                      color="primary"
                                      icon={<RadioButtonUnchecked />} />
                        </Grid>
                    </Grid>
                </FormGroup>

                <NumberStepperComponent max={3}
                                        min={0}
                                        onStepHandler={this.handleOnChange}
                                        step={1}
                                        value={this.props.saveCount} />
            </FormControl>
        );
    }
}

const characterDeathSavesFieldComponent = withStyles(characterDeathSavesStyles)(CharacterDeathSavesFieldComponent);

export {characterDeathSavesFieldComponent as CharacterDeathSavesFieldComponent};
