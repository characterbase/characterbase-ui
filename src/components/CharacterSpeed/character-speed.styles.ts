import {createStyles} from "@material-ui/core";

export const characterSpeedStyles = createStyles({
    characterSpeed: {
        padding: 8
    },
    characterSpeedField: {
        padding: 10
    }
});
