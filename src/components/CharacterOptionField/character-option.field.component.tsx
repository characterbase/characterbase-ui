import {FormControl, Input, InputLabel, MenuItem, Select} from "@material-ui/core";
import * as React from "react";

type Props = {
    currentValue?: string;
    label: string;
    options: Array<{ id: string; value: string; }>;
    readOnly: boolean;
    fullWidth?: boolean;
    onChangeHandler: (selectedValue: string) => void;
};

type State = {
    value: string;
};

export class CharacterOptionFieldComponent extends React.Component<Props, State> {
    public state: State;

    constructor(props: Props) {
        super(props);

        this.state = {
            value: this.props.currentValue || ""
        };
    }

    public handleOnChange = (event: React.FormEvent): void => {
        const eventTarget = event.target as HTMLSelectElement;

        this.setState(() => ({
            value: eventTarget.value
        }));

        this.props.onChangeHandler(eventTarget.value);
    }

    public compileOptions = (): JSX.Element[] => {
        return this.props
            .options
            .map(this.mapOptionToJsx);
    }

    public render(): JSX.Element {
        return (
            <FormControl fullWidth={this.props.fullWidth}>
                <InputLabel>{this.props.label}</InputLabel>

                <Select input={<Input readOnly={this.props.readOnly} />}
                        value={this.state.value}
                        onChange={this.handleOnChange}>
                    {this.compileOptions()}
                </Select>
            </FormControl>
        );
    }

    private mapOptionToJsx = (option: { id: string; value: string }): JSX.Element => {
        return (
            <MenuItem key={option.id}
                      value={option.id}>
                {option.value}
            </MenuItem>
        );
    }
}
