import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";

export const characterSheetStyles = (): StyleRules => createStyles({
    characterSheet: {
        flexGrow: 1
    },
    characterSheetGridItem: {
        padding: 10
    }
});
