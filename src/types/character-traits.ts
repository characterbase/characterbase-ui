export type CharacterTraits = {
    personalityTraits: string;
    ideals: string;
    bonds: string;
    flaws: string;
};
