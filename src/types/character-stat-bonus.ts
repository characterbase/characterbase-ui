type CharacterStatBonusSource =
    | "abilityScore"
    | "class"
    | "item"
    | "race"
    | "spell";

export type CharacterStatBonus = {
    modifier: number;
    permanent: boolean;
    source: CharacterStatBonusSource;
};
