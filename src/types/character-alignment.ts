export type CharacterAlignment = {
    id: string;
    name: string;
};
