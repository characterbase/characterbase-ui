export type CharacterHitPoints = {
    current: number;
    maximum: number;
    temporary: number;
};
