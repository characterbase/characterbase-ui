import {CharacterAbilityScores} from "./character-ability-scores";
import {CharacterAttack} from "./character-attack";
import {CharacterCurrency} from "./character-currency";
import {CharacterDeathSaves} from "./character-death-saves";
import {CharacterDetails} from "./character-details";
import {CharacterHitDice} from "./character-hit-dice";
import {CharacterHitPoints} from "./character-hit-points";
import {CharacterSavingThrows} from "./character-saving-throws";
import {CharacterSkills} from "./character-skills";
import {CharacterTraits} from "./character-traits";

export type Character = {
    abilityScores: CharacterAbilityScores;
    armorClass: number;
    attacks: CharacterAttack[];
    currency: CharacterCurrency;
    deathSaves: CharacterDeathSaves;
    details: CharacterDetails;
    hitDice: CharacterHitDice;
    hitPoints: CharacterHitPoints;
    id: string;
    initiative: number;
    inspiration: boolean;
    proficiencyBonus: number;
    savingThrows: CharacterSavingThrows;
    skills: CharacterSkills;
    speed: number;
    traits: CharacterTraits;
};
