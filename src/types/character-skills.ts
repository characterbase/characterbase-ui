import {CharacterStatBonus} from "./character-stat-bonus";

type CharacterSkillNames =
    | "acrobatics" | "animalHandling" | "arcana" | "athletics" | "deception" | "history"
    | "insight" | "intimidation" | "investigation" | "medicine" | "nature" | "perception"
    | "performance" | "persuasion" | "religion" | "sleightOfHand" | "stealth" | "survival";

export type CharacterSkill = {
    bonuses: CharacterStatBonus[];
    proficient: boolean;
};

export type CharacterSkills = {
    [key in CharacterSkillNames]: CharacterSkill;
};
