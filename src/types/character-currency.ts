export type CharacterCurrency = {
    copper: number;
    electrum: number;
    gold: number;
    platinum: number;
    silver: number;
};
